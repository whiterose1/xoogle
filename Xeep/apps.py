from django.apps import AppConfig


class XeepConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'Xeep'
